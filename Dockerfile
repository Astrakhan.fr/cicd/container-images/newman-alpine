FROM node:12-alpine

RUN apk add --no-cache bash curl jq
RUN npm install -g newman newman-reporter-html newman-reporter-htmlextra
